public class Driver implements Actions {
        private int id;
        private String name;

        //getters
        public int getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        //setters
        public void setId(int id) {
            this.id = id;
        }

        public void setName(String name) {
            this.name = name;
        }

        public void sit() {
            System.out.println("Can I drive standing?");
        }

        public void eat() {
            System.out.println("Only when I take breaks.");
        }
}
