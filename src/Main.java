import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class Main {
    // give an access modifier, set to public in this example.
    // set this method to static so that this method can be executed without instantiating an object from this class first.
    // void is used to indicate that this function / method will not return any values upon execution.
    // main is the method name.
        // the method named main will have a parameter named args which is an array of String values.
    public static void main(String[] args){
        //String is spelled as such because it is not a primitive D.T., it is a class from the language library of Java.
//        char gender = 'M';
//        String name = "Albert";
//        int age = 2;
//        boolean isEmployed = true;
//        double fare = 13.50;
//        System.out.println("Hi! my name is " + name + " I am a " + gender + " and I am currently " + age + " years old" + ". Am I employed? " + isEmployed + ". How much is my daily fare? " + fare);
//        declares an array strictly of 5 elements with int data type, no initialization.

//        int[] intArray = {11, 22, 33, 44, 55};
//        System.out.println(intArray[0]);
//        System.out.println(intArray[1]);
//        System.out.println(intArray[2]);
//        System.out.println(intArray[3]);
//        System.out.println(intArray[4]);

//        int[] intArray2 = new int[5];
//        System.out.println(intArray2[0]);
//        System.out.println(intArray2[1]);
//        System.out.println(intArray2[2]);
//        System.out.println(intArray2[3]);
//        System.out.println(intArray2[4]);

//        double[] doubleArray = {.11, .22, .33, .44, .55};
//        System.out.println(doubleArray[0]);
//        System.out.println(doubleArray[1]);
//        System.out.println(doubleArray[2]);
//        System.out.println(doubleArray[3]);
//        System.out.println(doubleArray[4]);
//
//        char[] charArray = {'A', 'B', 'C', 'D', 'E'};
//        System.out.println(charArray[0]);
//        System.out.println(charArray[1]);
//        System.out.println(charArray[2]);
//        System.out.println(charArray[3]);
//        System.out.println(charArray[4]);
//
//        boolean[] booleanArray = {true, false, true, false, true};
//        System.out.println(booleanArray[0]);
//        System.out.println(booleanArray[1]);
//        System.out.println(booleanArray[2]);
//        System.out.println(booleanArray[3]);
//        System.out.println(booleanArray[4]);
//
//        String[] StringArray = {"AAA", "BBB", "CCC", "DDD", "EEE"};
//        System.out.println(StringArray[0]);
//        System.out.println(StringArray[1]);
//        System.out.println(StringArray[2]);
//        System.out.println(StringArray[3]);
//        System.out.println(StringArray[4]);
        // Instantiate a new object from the Scanner class in the Java utility library.
        // Initialize this new object with user input from the terminal obtained via the in property of the System class.
//        Scanner sc = new Scanner(System.in);
//        System.out.println("Input year to determine whether it's a leap year or not: ");
//        int year = sc.nextInt();
//        System.out.println("You gave me " + year);
//
//        boolean isDivisibleByFour = year % 4 == 0;
//        boolean isDivisibleByFourHundred = year % 400 == 0;
//        boolean isDivisibleByOneHundred = year % 100 == 0;
//        boolean isNotDivisibleByOneHundred = year % 100 != 0;
//
//        if (isDivisibleByFour && isNotDivisibleByOneHundred) {
//            System.out.println("You got a leap year.");
//        }
//        else if (isDivisibleByOneHundred && isDivisibleByFourHundred) {
//            System.out.println("You got a leap year.");
//        }
//        else {
//            System.out.println("It's not a leap year.");
//        }

//        ArrayList<String> friends = new ArrayList<String>();
//        friends.add("Terence");
//        friends.add("Alan");
//
//        System.out.println("I am friends with " + friends.get(1));
//
//        friends.set(1, "Georgie");
//        System.out.println(friends.get(1));
//
//        friends.set(1, "Mr. Beraquit");
//        System.out.println("I am friends with " + friends.get(1));
//
//        //remove an element from an ArrayList via the remove() method
//        friends.remove(0);
//        System.out.println("I am friends with " + friends.get(0));
//
//        // remove all elements via the clear () method
//
//        System.out.println(friends.size());

//        HashMap<String, Integer> inventory = new HashMap<String, Integer>();
//        inventory.put("Alcohol", 20);
//        inventory.put("TP", 15);
//        inventory.put("Soap", 5);
//        inventory.put("Hotdog", 8);
//
//        System.out.println(inventory);
//        System.out.println("I currently have " + inventory.get("Alcohol") + " bottles of alcohol left.");
//        inventory.remove("Hotdog");
//        System.out.println(inventory);
//
//        System.out.println(inventory.keySet());

//        String[] instructors = {"Ben", "Alan", "Terence"};
//        for (String name:instructors
//             ) {
//            System.out.println("Hi, my name is " + name);
//        }
        // using a while loop or a for loop, calculate for the factorial of a given number
//        Scanner input = new Scanner(System.in);
//        System.out.println("Red, Black, Blue, Yellow");
//
//        String directionValue = input.next();
//
//        switch (directionValue){
//            case "Red":
//                System.out.println("Red = U");
//                break;
//            case "Black":
//                System.out.println("Black = R");
//                break;
//            case "Blue":
//                System.out.println("Blue = D");
//                break;
//            case "Yellow":
//                System.out.println("Yellow = L");
//                break;
//            default:
//                System.out.println("Invalid Option");
//                break;
//        }
        Car myCar = new Car();
        System.out.println(myCar.getDriverName());
    }
}
