public class Minimum {

    public static void main(String[] args){

        int[] number = {};

        System.out.println(minimum(new int[]{10, 20, 3, 40, 5000, 9000, 1000000}));
    }

    public static int minimum(int[] numbers){
        int min = 50000000;
        for (int number: numbers
        ){
            if (number < min){
                min = number;
            }
        }

        return min;
    }
}
