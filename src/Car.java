public class Car {
    // has a relationship with class Driver
    private Driver driver;
    //driver is an instance of class driver.

    //create a default constructor for our class Car.
    Car() {
        //Whenever I instantiate a new care object from the Car class, it has to have a driver.
        this.driver = new Driver();
        //instantiates a new driver instance / object AND assigns this new instance to the driver property.
        //invoke the setName setter method inherited from the driver class, to set then name of the newly instantiated driver object to "Alexandro"
        driver.setName("Alexandro");
    }
    // define a getter method for this car's driver name

    public String getDriverName(){
        //invoke the inherited getter method from the Driver class where the driver property of this car was instantiated from.
        return driver.getName();
    }
}
