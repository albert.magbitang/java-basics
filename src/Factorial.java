import java.util.Scanner;

public class Factorial {
    public static void main(String[] args){
        // using a while loop or a for loop, calculate for the factorial of a given number.
        System.out.println("Input a number whose factorial will be computed: ");
        Scanner scanner = new Scanner(System.in);
        int number = scanner.nextInt();
        System.out.println("You've input " + number);

    int i, factorial=1;
    for (i=1; i <= number; i++){
        factorial=factorial*i;
    }
        System.out.println("The factorial of: " + number + " is: " + factorial);

//        int i, no=1;
//        for (i=1; i <= number; i++){
//            no=no*i;
//        }
//        System.out.println("Factorial of: " + number + " is: " + no);

//        int i
    }
}
