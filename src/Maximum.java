public class Maximum {

    public static void main(String[] args){

        int[] number = {10, 20, 3, 40, 5000, 9000, 1000000};

        System.out.println(maximum(new int[]{10, 20, 3, 40, 5000, 9000, 1000000}));
    }

    public static int maximum(int[] numbers){
        int max = 0;
        for (int number: numbers
            ){
            if (number > max){
                max = number;
            }
        }

        return max;
    }
}
