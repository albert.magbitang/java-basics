public class Passenger implements Actions{
        private String name;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public void sit() {
            System.out.println("Of course, I'm sitting.");
        }

        public void eat() {
            System.out.println("The driver kicked me out.");
        }
}
